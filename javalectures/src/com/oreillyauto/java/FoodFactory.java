package com.oreillyauto.java;

public class FoodFactory {

    FoodFactory factory; // Singleton instance of this class
    
    public FoodFactory getFactory() {
        if (factory == null) {
            factory = new FoodFactory();
        }
        
        return factory;
    }

}
