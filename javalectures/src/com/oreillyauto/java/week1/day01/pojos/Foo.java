package com.oreillyauto.java.week1.day01.pojos;

import java.math.BigDecimal;

public class Foo {

	public static void main(String[] args) {
		//Foo foo = new Foo();
	    String name = "Jeffery";
	    name = "Jeffery Brannon";
	    System.out.println(name);
	    
	    BigDecimal amount = new BigDecimal("25000");
	    amount = amount.add(new BigDecimal("75000"));
	    System.out.println(amount);
	    
	}

}
