package com.oreillyauto.java.week1.day01;

public class SmartClock extends SmartProtectedClock {
    public long getTimeInSeconds() {
        return this.time / 1000;
    }
}
