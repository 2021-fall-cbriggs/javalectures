package com.oreillyauto.java.week1.day04.interfaces;

public interface AnotherInterface {
    public void doStuff();
    public void dontDoStuff();
    
    //@Override
    default String doSomethingElse(String x, String y) {
        return "x:" + x + " and y: " + y;
    }
    
}
