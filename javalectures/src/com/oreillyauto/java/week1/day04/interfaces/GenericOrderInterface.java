package com.oreillyauto.java.week1.day04.interfaces;

import java.util.Date;

public interface GenericOrderInterface {
	public void showNumberOfItems(int numberOfItems);
	public void showOrderType();
	
	default void showDate() {
		System.out.println(new Date());
	}

}
