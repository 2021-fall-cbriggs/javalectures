package com.oreillyauto.java.week1.day04;

class Reptile extends Animal {
	
	public String animalTime() {
		return "It's reptile time!";
	}

	public String invokeSuperTime() {
		return super.animalTime();
	}
}