package com.oreillyauto.java.week1.day04;

public class Animal {
	public int i;
	public String j;
	int debug = 0;
	
	Animal() {
		if (debug > 0) System.out.println("Animal Empty Constructor called!");
	}
	
	public Animal(int x, String y) {
		this.i = x;
		this.j = y;
		if (debug > 0) System.out.println("Animal Constructor(int, String) called!");
	}

	public String animalTime() {
		return "It's animal time!";
	}

	// getters and setters
	public int getI() {
		return i;
	}

	public void setI(int i) {
		this.i = i;
	}

	public String getJ() {
		return j;
	}

	public void setJ(String j) {
		this.j = j;
	}

}
