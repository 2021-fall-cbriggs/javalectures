package com.oreillyauto.java;

public class ThisExample {

    private static ThisExample thisExample = null;
    
    public ThisExample() {
        
    }

    public ThisExample getThis() {
        return this;
    }
    
    private static ThisExample getThisExampleInstance() {
        return thisExample;
    }
    
    public static void main(String[] args) {
        thisExample = new ThisExample();
        System.out.println(getThisExampleInstance());
    }
    
    @Override
    public String toString() {
        return "you got this (instance)!";
    }
    
}
