package com.oreillyauto.java.week2.helpers;

public class StringHelper {
    
    public static String reverseString(String string) {
        if ("".equals(string)) {
            return "";
        } else if (string == null) {
            return null;
        } else {
            return new StringBuilder().append(string).reverse().toString();    
        }
    }
    
    
}
