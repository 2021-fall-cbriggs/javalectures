package com.oreillyauto.java.week2.day01;

// Cannot extend a *final* class
// The type TeamnetCTT cannot subclass the final class Teamnet
public class TeamnetCTT /*extends Teamnet*/ { 
    public TeamnetCTT() {
    }
}
