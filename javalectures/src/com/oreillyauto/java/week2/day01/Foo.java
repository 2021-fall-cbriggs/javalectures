package com.oreillyauto.java.week2.day01;

import java.io.File;

public class Foo {

    public Foo() {
        // TODO Auto-generated constructor stub
    }

    public static void main(String[] args) {
        for (String string : args) {
            File file = new File(string);
            
            System.out.println("File ("+string+") exists => " + file.exists());
        }
    }

}
