package com.oreillyauto.java.week2.classes;

public class ThreadThatExtends extends Thread {

    public void run() {
        try {
        	// Thread#getId():
        	// Returns the identifier of this Thread. The thread ID 
        	// is a positive long number generated when this thread 
        	// was created. The thread ID is unique and remains unchanged 
        	// during its lifetime. When a thread is terminated, this 
        	// thread ID may be reused.
        		
            // Displaying the thread that is running
            System.out.println ("Thread " + Thread.currentThread().getId() + " is running");
        } catch (Exception e) {
            // Throwing an exception
            System.out.println ("Exception is caught");
        }
    }
}
