package com.oreillyauto.java.week2.classes;

public class ExampleThread extends Thread {
	private int delay;

	public ExampleThread(String name, int delay) {
		// Give this particular thread a name:
		// "thread <'LABEL'>".
		super("thread '" + name + "'");
		this.delay = delay;
	}

	public void run() {
		for (int count = 1; count < 20; count++) {
			try {
				System.out.format("Line #%d from %s\n", count, getName());
				Thread.sleep(delay);
			} catch (InterruptedException ie) {
				// This would be a surprise.
				// Don't swallow the exception. Print it.
				ie.printStackTrace();
			}
		}
	}
}
