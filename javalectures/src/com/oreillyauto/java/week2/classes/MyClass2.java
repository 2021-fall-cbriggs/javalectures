package com.oreillyauto.java.week2.classes;

public class MyClass2 {

    public int incrementing = 0;
    public int nonStaticVariable = 0;

    public MyClass2() {
        System.out.println("MyClass2 instantiated");
    }

    public static String getInfoFromStaticMethod() {
        return "foo";
    }

    public static class MyInnerClass {
        public static String getInnerMethod() {
            return "foo2";
        }
    }

    public class MyInnerClass2 {
        public String getInnerMethod2() {
            return "foo2";
        }
    }
}
