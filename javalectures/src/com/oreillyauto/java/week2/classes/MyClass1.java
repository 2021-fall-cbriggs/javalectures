package com.oreillyauto.java.week2.classes;

public class MyClass1 {
    
    protected static int nonStaticVariable  = 0;
    
    public MyClass1() {
        MyInnerClass mic = new MyInnerClass();
        System.out.println(mic.getInnerStringMethod()); 
        System.out.println(MyInnerClass.getInnerMethod());
    }
    
    private static class MyInnerClass {
        protected static final int getInnerMethod() {
            int increment = nonStaticVariable += 1;
            return increment;    
        }
        protected final String getInnerStringMethod() {
            return "Hello World!";
        }
    }
    
    public static void main(String[] args) {
        new MyClass1();
    }
}